export class Commentator {
    oldRoom;
    interval;
    constructor(io) {
        this.io = io;
        this.factory = new Factory();
    }

    emit(idRoom, name, newRoom, ...args) {
        this.idRoom = idRoom;
        this.io.to(idRoom).emit(name, ...args);
        const newName = this.getNewName(name, newRoom);
        const message = this.factory.create(newName)?.message(newRoom);
        if (message) this.io.to(idRoom).emit("MESSAGE", message);

        this.oldRoom = JSON.parse(JSON.stringify(newRoom));
        if (name === "START") {
            this.interval = setInterval(this.sendState, 30 * 1000);
        } else if (name === "GAME_OVER") {
            clearInterval(this.interval);
        }
    }
    sendState = () => {
        const message = this.factory.create("STATE")?.message(this.oldRoom);
        this.io.to(this.idRoom).emit("MESSAGE", message);
    };
    getNewName(name, newRoom) {
        if (name !== "UPDATE_ROOM") {
            return name;
        } else {
            const someoneToClose = newRoom.Users.some((user, index) => {
                return user.Progress >= 80 &&
                    this.oldRoom.Users[index].Progress < 80 &&
                    user.Name === this.oldRoom.Users[index].Name;
            });
            const someoneFinish = newRoom.Users.some((user, index) => {
                return user.Progress === 100 &&
                    this.oldRoom.Users[index].Progress < 100 &&
                    user.Name === this.oldRoom.Users[index].Name;
            });
            if (someoneFinish) {
                return "SOMEONE_FINISH";
            } else if (someoneToClose) {
                return "SOMEONE_TO_CLOSE";
            } else {
                return null;
            }
        }
    }
}

class Factory {
    create(type) {
        switch (type) {
            case "READY_TO_START":
                return new READY_TO_START();
            case "GAME_OVER":
                return new GAME_OVER();
            case "UPDATE_ROOM":
                return new UPDATE_ROOM();
            case "START":
                return new START();
            case "SOMEONE_TO_CLOSE":
                return new SOMEONE_TO_CLOSE();
            case "SOMEONE_FINISH":
                return new SOMEONE_FINISH();
            case "STATE":
                return new STATE();
            default:
                return null;
        }
    }
}
class SOMEONE_FINISH {
    message(room) {
        const finishUser = room.Users.filter(user => user.Progress === 100).sort(
            (a, b) => a.Result - b.Result
        )[0];
        return `${finishUser.Name} заканчивает гонку и может теперь спокойно отдохнуть!`;
    }
}
class SOMEONE_TO_CLOSE {
    message(room) {
        const closeUser = room.Users.filter(user => user.Progress >= 80).sort(
            (a, b) => a.Progress - b.Progress
        )[0];
        return `А вот и ${closeUser.Name} уже на финишной прямой!`;
    }
}
class START {
    message(room) {
        return `Вот и рванули наши гонщики в составе ${room.Users.length} человек`;
    }
}
class STATE {
    message(room) {
        return (
            `На данный момент рейтинг таков: ${room.Users.sort(
                (a, b) => b.Result - a.Result
            )
                .map((user, index) => index + 1 + ". " + user.Name)
                .join(", ")}. ` + `Посмотрим поменяется ли картина до конца игры.`
        );
    }
}
class READY_TO_START {
    message(room) {
        return (
            ` Итак, все гонщики уже на старте, а значит всего через несколько ` +
            `секунд мы начинаем! Давайте познакомимся с участниками: ${room.Users.map(
                (user, index) => index + 1 + ". " + user.Name
            ).join(", ")}`
        );
    }
}
class GAME_OVER {
    message(room) {
        const users = room.Users;
        let text = `Вот єто финал! `;
        if (users.every((user) => user.Progress === 100))
            text += `Даже время не успело закончиться! `;
        text += `Безоговорочным лидером сегодняшней гонки стает ${users[0].Name}. `;
        if (users[1]) text += `Серебро достается ${users[1].Name}. `;
        if (users[2]) text += `Ну и замыкает тройку лидеров ${users[2].Name}. `;
        return text;
    }
}
